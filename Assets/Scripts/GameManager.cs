using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{

    [SerializeField] GameObject player;
    [SerializeField] GameObject goal;
    [SerializeField] Transform[] milestones;
    float roadLenght;
    float passedLenght;
    Vector3 startPoint;
    Vector3 goalPoint;
    public int progress { get; private set; }

    void Start()
    {
        progress = 0;
        startPoint = player.transform.position;
        goalPoint = goal.transform.position;
        roadLenght = GetRoadLenght();
    }

    void Update()
    {
        passedLenght = GetPassedLenght();
        progress = (int)(passedLenght * 100 / roadLenght);
    }

    float GetRoadLenght()
    {
        float lenght = Vector3.Distance(startPoint, milestones[0].position);

        for(int i = 0; i < milestones.Length -1; i++)
        {
            lenght += Vector3.Distance(milestones[i].position, milestones[i + 1].position);
        }

        lenght += Vector3.Distance(milestones[milestones.Length - 1].position, goalPoint);
        return lenght;
    }

    /*����� ���������� ����� ����������� ������������ �������,
     *� ���������� ����� ��������� ���������� ������ � �������*/
    float GetPassedLenght()
    {
        float lenght = 0f;
        float distanceToPlayer = 0f;
        float betweenPoints = 0f;
        Vector3 prePoint = startPoint;
        Vector3 nextPoint = new Vector3(0, 0, 0);

        for (int i = 0; i < milestones.Length; i++)
        {
            nextPoint = milestones[i].position;
            betweenPoints = Vector3.Distance(prePoint, nextPoint);
            distanceToPlayer = Vector3.Distance(prePoint, player.transform.position);

            if(betweenPoints < distanceToPlayer)
            {
                lenght += betweenPoints;
                prePoint = nextPoint;
            }
            else
            {
                lenght += distanceToPlayer;
                return lenght;
            }

        }

        lenght += Vector3.Distance(nextPoint, player.transform.position);
        return lenght;

    }
}
