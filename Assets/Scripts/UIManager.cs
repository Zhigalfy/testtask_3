using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    [SerializeField] GameObject title;
    [SerializeField] GameObject pauseMenu;
    [SerializeField] GameObject gameMenu;
    float roundTime;
    string roundTimeString;
    [SerializeField] PlayerController playerController;
    [SerializeField] GameManager gameManager;
    [SerializeField] Text timeText;
    [SerializeField] Text tensionText;
    [SerializeField] Button pauseButton;
    [SerializeField] Text titleText;
    [SerializeField] Text progressText;

    private void Awake()
    {
        Time.timeScale = 0f;
    }

    // Start is called before the first frame update
    void Start()
    {
        title.gameObject.SetActive(true);
        pauseMenu.gameObject.SetActive(false);
        gameMenu.gameObject.SetActive(false);
        Time.timeScale = 0f;
    }

    // Update is called once per frame
    void Update()
    {
        roundTime += Time.deltaTime;
        roundTimeString = roundTime.ToString("F4");
        timeText.text = $"�����: {roundTimeString}";
        progressText.text = $"�������� {gameManager.progress} %";
        tensionText.text = $"���� ��������� {playerController.tension} %";

        if (playerController.isGoal)
        {
            title.gameObject.SetActive(true);
            pauseMenu.gameObject.SetActive(false);
            gameMenu.gameObject.SetActive(false);
            Time.timeScale = 0f;
            titleText.text = $"��������� �����������\n�����: {roundTimeString}\n���������� �����: {playerController.tryCount}"; 
        }
    }

    public void PlayGame()
    {
        title.gameObject.SetActive(false);
        pauseMenu.gameObject.SetActive(false);
        gameMenu.gameObject.SetActive(true);
        pauseButton.gameObject.SetActive(true);
        Time.timeScale = 1f;
        roundTime = 0f;
        playerController.ResetPosition();
    }

    public void PauseGame()
    {
        title.gameObject.SetActive(false);
        pauseMenu.gameObject.SetActive(true);
        gameMenu.gameObject.SetActive(true);
        pauseButton.gameObject.SetActive(false);
        Time.timeScale = 0f;
    }

    public void ContinueGame()
    {
        title.gameObject.SetActive(false);
        pauseMenu.gameObject.SetActive(false);
        gameMenu.gameObject.SetActive(true);
        pauseButton.gameObject.SetActive(true);
        Time.timeScale = 1f;
    }
        
    public void ExitToMenu()
    {
        title.gameObject.SetActive(true);
        pauseMenu.gameObject.SetActive(false);
        gameMenu.gameObject.SetActive(false);
        Time.timeScale = 0f;
    }

}

