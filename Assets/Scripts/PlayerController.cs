using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    float force = 10.0f;
    Vector2 touchStartPosition;
    bool isDragging;
    Rigidbody playerRB;
    Vector3 moveDir;
    Vector2 pos;
    Vector2 deltaDir;
    float deltaDist;
    public int tension { get; private set; }
    public bool isGoal { get; private set; }
    float forceMultiplier;
    float degrees;
    [SerializeField] GameObject arrow;
    RectTransform arrowRect;
    Vector3 baseArrowScale;
    Vector3 checkpointPosition;
    Quaternion checkpointRotation;
    Vector3 basePosition;
    Quaternion baseRotation;
    bool isMoving;
    public int tryCount { get; private set; }
    float updatedRotation;
    float minimumPlayerSpeed;

   
    void Start()
    {
        playerRB = GetComponent<Rigidbody>();
        basePosition = transform.position;
        baseRotation = transform.rotation;
        checkpointPosition = basePosition;
        checkpointRotation = baseRotation;
        moveDir = new Vector3(0,0,0);
        arrowRect = arrow.gameObject.GetComponent<RectTransform>();
        baseArrowScale = arrowRect.localScale;
        arrow.gameObject.SetActive(false);
        isMoving = false;
        minimumPlayerSpeed = 0.01f;
    }

    private void FixedUpdate()
    {
        if (forceMultiplier >= 1) // �������� �������� ��� ��������� ������� �� �����
        {
            // ������ ��������
            transform.rotation = Quaternion.Euler(0f, updatedRotation, 0f);
            playerRB.AddForce(transform.forward * force * forceMultiplier);
            tryCount++;
            tension = 0;
            forceMultiplier = 0f;
        }
           
        // ����������� ��������� ��� ����� ��������� ��������
        if (playerRB.velocity.magnitude >= minimumPlayerSpeed)
        {
            isMoving = true;
        }

        if (playerRB.velocity.magnitude < minimumPlayerSpeed && isMoving)
        {
            isMoving = false;
            checkpointPosition = transform.position;
            checkpointRotation = transform.rotation;
        }

    }

    void Update()
    {
        // ����������� ������ ��� ������
        if (transform.position.y <= 0f){
            playerRB.velocity = Vector3.zero;
            playerRB.angularVelocity = Vector3.zero;
            transform.position = checkpointPosition;
            transform.rotation = checkpointRotation;
        }
        TouchController();
    }

    void StartTouch()
    {
        touchStartPosition = Input.mousePosition;
        isDragging = true;
    }

    void Dragging()
    {
        pos = Input.mousePosition;
        deltaDir = (pos - touchStartPosition).normalized;
        deltaDist = Vector2.Distance(touchStartPosition, pos);

        // ������������ ��������� ����� �������� ������ ������
        if (deltaDist > Screen.height / 2)
        {
            deltaDist = Screen.height / 2;
        }

        if (deltaDir.y < 0) //������� ���������� ����� �������� ��������� ����
        {
            moveDir = new Vector3(-deltaDir.x, 0f, -deltaDir.y);
            arrow.gameObject.SetActive(true);
            degrees = Mathf.Acos(moveDir.x) * (180 / Mathf.PI) - 90;  //������� �� ���������� ������� � �������
            updatedRotation = transform.rotation.eulerAngles.y - degrees;
            arrowRect.rotation = Quaternion.Euler(90, 0, 90 - updatedRotation); // ������� �������
            float touchDistance = deltaDist / (Screen.height / 2);
            tension = (int)(touchDistance * 100);
            float updatedArrowLenght = baseArrowScale.x + touchDistance;
            arrowRect.localScale = new Vector3(updatedArrowLenght, baseArrowScale.y, baseArrowScale.z); // ��������� ����� �������
        }
        else
        {
            degrees = 0f;
            tension = 0;
            arrow.gameObject.SetActive(false);
        }
    }

    void EndTouch()
    {
        isDragging = false;
        forceMultiplier = tension;
        arrow.gameObject.SetActive(false);
    }

    void TouchController()
    {
        if (Input.GetMouseButtonDown(0) && !isMoving)
        {
            StartTouch();
        }

        else if (Input.GetMouseButton(0) && isDragging)
        {
            Dragging();
        }

        else if (Input.GetMouseButtonUp(0))
        {
            EndTouch();
        }
    }

    public void ResetPosition()
    {
        playerRB.velocity = Vector3.zero;
        playerRB.angularVelocity = Vector3.zero;
        transform.position = basePosition;
        transform.rotation = baseRotation;
        isGoal = false;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Goal"))
        {
            isGoal = true;
        }
    }
}
